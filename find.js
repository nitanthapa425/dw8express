

// for normal searching

//total 9
[
    {name:"nitan",age:29, isMarried:false}, =    *
    {name:"sandip",age:25, isMarried:false},      
    {name:"nitan",age:26, isMarried:true},        
    {name:"rishav",age:20, isMarried:false},       
    {name:"nitan",age:29, isMarried:true}, =
    {name:"chhimi",age:15, isMarried:true},        
    {name:"narendra",age:27, isMarried:false},    
    {name:"shidhant",age:16, isMarried:false},     
    {name:"kriston",age:22, isMarried:false},      
]



// find({
//   $and:[
//       {name:"nitan",age:29},
//       {age:29, isMarried:false},     
//       {name:"chimmi",age:19}
//   ]
// })

//in searching type does not matter
// find({})
// find({name:nitan})
// find({age:29})
// find({age:"29"})
// find({isMarried:"true"})


/*

find({name:"nitan", age:29})
find({name:"nitan", age:29 , isMarried:"true"})
find({age:20})
find({age:{$in:[20,15,16]}})
find({age:{$ne:20}})
find({age:{$gt:20}})
find({age:{$gte:20}})
find({age:{$lt:20}})
find({age:{$lte:20}})
and or

find({
$and:[
    {age:29},
    {isMarried:false}

]
})

find({
$or:[
    {name:"nitan"}
    {age:29}
]
})

25 -28

find({
    $and:[
        {age:{$gte:25}}
        {age:{$lte:28}}
    ]
})


[50 , 100]'

if(age>=50&& age<=100)



find({
    $and:[
        {name:"nitan",age:29},
        {age:29, isMarried:false},     
        {name:"chimmi",age:19}
    ]
})








*/




//for  regex searchin
[
   {name:"ni1t",age:29, isMarried:false},
    {name:"sand2inip",age:25, isMarried:false},
    {name:"ni",age:26, isMarried:true},*
    {name:"ris3hav",age:20, isMarried:false},
    {name:"nitan",age:29, isMarried:true},
    {name:"chhimi",age:15, isMarried:true},
    {name:"narendran",age:27, isMarried:false},
    {name:"Nitan",age:16, isMarried:false},
    {name:"nitanthapa",age:22, isMarried:false},
]




find({name:/^[A-Z].*[0-9]$/})

 

find({name:"nitan"})//exact searching
find({name:/nitan/})// regex searching => not exact searching
find(name:/nitan/i)
find(name:/ni/)
find(name:/^ni/)
find(name:/^ni/i)
find(name:/n$/i)
find(name:/^(?=.*[a-zA-Z])(?=.*\d).+/)

find(name:/^(?=.*@)(?=.*_)|(?=.*_)(?=.*@)/)





//for array and object searching
[
    //searching
    //search according field
  
    {
      name: "nitan",
      location: {
        country: "nepal",
        exactLocation: "gagal",
      },
      favTeacher: ["bhishma", "nitan", "ram"],
      favSubject: [
        {
          bookName: "C",
          bookAuthor: "nitan",
        },
        {
          bookName: "javascript",
          bookAuthor: "nitan",
        },
      ],
    },
    {
      name: "ram",
      location: {
        country: "china",
        exactLocation: "sikhu",
      },
      favTeacher: ["hari", "nitan", "ram"],
      favSubject: [
        {
          bookName: "C",
          bookAuthor: "nitan",
        },
        {
          bookName: "javascript",
          bookAuthor: "nitan",
        },
      ],
    },
    {
      name: "shyam",
      location: {
        country: "india",
        exactLocation: "bihar",
      },
      favTeacher: ["niti", "nitan", "ram"],
      favSubject: [
        {
          bookName: "C",
          bookAuthor: "nitan",
        },
        {
          bookName: "javascript",
          bookAuthor: "nitan",
        },
      ],
    },
  ];
  
  //find({name:"nitan"})
  // find({ "location.country": "nepal" });
  // find({favTeacher:"bhishma"})
  //find({"favSubject.bookAuthor":"nitan"})
  

  //find it determine which object to show or not
  //select 
  //sort
  //limit
  //skip


//for sorting
[
    {name:"ac",age:29, isMarried:false},
    {name:"b",age:40, isMarried:false},
    {name:"ab",age:50, isMarried:false},
    {name:"ab",age:60, isMarried:false},
    {name:"c",age:40, isMarried:false},
  
]

// find({}).sort("name") => ascending sort 
// find({}).sort("-name") => descending sort
// find({}).sort("name age") => 
// find({}).sort("name -age") =>
// find({}).sort("age name")


//output
[

  {name:"ac",age:29, isMarried:false},
  {name:"b",age:40, isMarried:false},
  {name:"c",age:40, isMarried:false},
  {name:"ab",age:50, isMarried:false},
  {name:"ab",age:60, isMarried:false},

]





  





  

 

]


//number sorting work properly unlike javascript
// find({}).sort("name")
// find({}).sort("-name")
// find({}).sort("name age")
// find({}).sort("name -age")

// //ascending sort  descending sort
// 

// find({}).sort("-name age")

// find({}).sort("age -name")


//skip
[
    {name:"nitan",age:29, isMarried:false},
    {name:"sandip",age:25, isMarried:false},
    {name:"nitan",age:26, isMarried:true},
    {name:"rishav",age:20, isMarried:false},
    {name:"nitan",age:29, isMarried:true},
    {name:"chhimi",age:15, isMarried:true},
    {name:"narendra",age:27, isMarried:false},
    {name:"shidhant",age:16, isMarried:false},
    {name:"kriston",age:22, isMarried:false},
]

//output
find({}).skip("2")
[
  {name:"nitan",age:26, isMarried:true},
  {name:"rishav",age:20, isMarried:false},
  {name:"nitan",age:29, isMarried:true},
  {name:"chhimi",age:15, isMarried:true},
  {name:"narendra",age:27, isMarried:false},
  {name:"shidhant",age:16, isMarried:false},
  {name:"kriston",age:22, isMarried:false},

]





//limit
[
    {name:"nitan",age:29, isMarried:false},
    {name:"sandip",age:25, isMarried:false},
    {name:"nitan",age:26, isMarried:true},
    {name:"rishav",age:20, isMarried:false},
    {name:"nitan",age:29, isMarried:true},
    {name:"chhimi",age:15, isMarried:true},
    {name:"narendra",age:27, isMarried:false},
    {name:"shidhant",age:16, isMarried:false},
    {name:"kriston",age:22, isMarried:false},
]

// find({}).limit("4")

//outptu
[
  {name:"nitan",age:29, isMarried:false},
  {name:"sandip",age:25, isMarried:false},

]






//skip and limit
[
    {name:"nitan",age:29, isMarried:false},
    {name:"sandip",age:25, isMarried:false},
    {name:"nitan",age:26, isMarried:true},
    {name:"rishav",age:20, isMarried:false},
    {name:"nitan",age:29, isMarried:true},
    {name:"chhimi",age:15, isMarried:true},
    {name:"narendra",age:27, isMarried:false},
    {name:"shidhant",age:16, isMarried:false},
    {name:"kriston",age:22, isMarried:false},
]

//output
[
  {name:"nitan",age:26, isMarried:true},
    {name:"rishav",age:20, isMarried:false},
    {name:"nitan",age:29, isMarried:true},
    {name:"chhimi",age:15, isMarried:true},
    {name:"narendra",age:27, isMarried:false},
 
]

find, select, sort, limit , skip

find({}).limit("3").skip("2")
[
  {name:"nitan",age:29, isMarried:true},
]



//this order works
//find , sort, select, skip, limit




//find({})
//find has control over the object



// 1, 2,    3,4,    5,6,   7,8,    9,10

// brake =x
// page =y

// 5,6





