import { Schema } from "mongoose";

//defining content of object
let studentSchema = Schema({
  name: {
    type: String,
    required: [true, "name field is required."],
  },

  age: {
    type: Number,
    required: [true, "age field is required."],
  },
  isMarried: {
    type: Boolean,
    required: [false],
  },
});

export default studentSchema;
