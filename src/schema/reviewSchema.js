import { Schema } from "mongoose";

//defining content of object
let reviewSchema = Schema({
  userId: {
    type: Schema.ObjectId,
    ref:"User",
    required: [true, "reviewId field is required."],
  },
  productId: {
    type: Schema.ObjectId,
    ref:"Product",
    required: [true, "productId field is required."],
  },
  description: {
    type: String,
    required: [true,"description field is required."],
  },
});

export default reviewSchema;
