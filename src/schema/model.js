//defining array

import { model } from "mongoose";
import studentSchema from "./studentSchema.js";
import teacherSchema from "./teacherSchema.js";
import pupilSchema from "./PupilSchema.js";
import productSchema from "./productSchema.js";
import userSchema from "./userSchema.js";
import reviewSchema from "./reviewSchema.js";

// singular + firstLetter capital
export let Student = model("Student", studentSchema);
export let Teacher = model("Teacher", teacherSchema);
export let Pupil = model("Pupil", pupilSchema);
export let Product = model("Product", productSchema);
export let User = model("User", userSchema);
export let Review = model("Review", reviewSchema);

//Student api
//localhost:8000/students
// post
// get
// update
// delete
