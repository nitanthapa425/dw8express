import { Schema } from "mongoose";

//schema

//manipulation
//uppercase
/* lowercase, trim default */

//validation
/* 
required
unique
minLength
maxLength
 */


let pupilSchema = Schema({
    name:{
        type : String,
        // uppercase:true,
        // lowercase:true,
        // trim:true,
        minLength:[5,"name must be at least 5 character long."],
        maxLength:[25, "name must be at most 25 character."],
        required : [true,"Name field is empty."],
        validate:(value)=>{

          if(/^[A-Za-z]+$/.test(value))
          {

          }
          else
          {
            throw new Error("name must contain only alphabet.")
          }

        }
    },
    age:{
        type: Number,
        required : [true,"Age is required."]
    },
    phoneNumber:{
        type: Number,
        required : [true,"PhoneNumber is required."],

        validate:(value)=>{
          let str= value.toString()
          console.log(value.length)

          if(str.length!==10)
          {
            throw new Error("phone number must be exact 10 character")
          }

        }

    },
    roll:{
        type: Number,
        min:[1,"roll number must be at least 1"],
        max:[100, "roll number must be at most 100"],
        required : [true,"Roll is required."]
    },
    isMarried:{
        type: Boolean,
        required : [true,"isMarried is required."]
    },
    spouseName: {
        type: String,
        required : [false]

    },
    email:{
        type: String,
        // unique:true,
        required : [true,"Email is required."],
        validate :(value)=>{
          if(value==="aavashpoudyal12344321@gmail.com")
          {
            throw new Error("this email is not authorized to register in our system")
          }
        }

    },
    gender:{
        type: String,
        default:"male",
        required : [true,"Gender is required."]
    },
    dob:{
        type: Date,
        required : [true,"Dob is required."]
    },
    location:{
        country:{
            type:String,
        required : [true,"Country is required."],
    },
        exactLocation:{
            type:String,
        required : [true,"Exact location is required."],
    }
   
    },
    favTeacher:[
        {
        type:String,
        required : [true,"Favorite teacher is required."],
      },
        
    ],
    favSubject:[{
        bookName:{
            type:String,
        required : [true,"Favorite subject is required."],
    },
        bookAuthor:{
            type:String,
        required : [true,"Book Author is required."]
    }}
   
    ],

})

export default pupilSchema





