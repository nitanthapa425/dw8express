import mongoose from "mongoose";

let connectToMongoDb = async () => {
  try {
    await mongoose.connect("mongodb://0.0.0.0:27017/dw8");
    console.log("application is connected to database successfully.");
  } catch (error) {
    console.log("unable to connect database");
  }
};

export default connectToMongoDb;
