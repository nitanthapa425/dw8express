import { Router } from "express";

let secondRouter = Router();

//middleware
// it is the function
// which has req, res , next

//for one request we must have only one response

// Based on error
// error middleware
// (err,req,res,next)=>{}
// next(value)

// without error middleware
// (req,res,next)=>{}
// to trigger next middleware  next()


//Based on location
// route middleware
// application middleware




secondRouter
  .route("/") //localhost:8000/seconds
  .post(
    (req, res, next) => {
      console.log("i am middleware 1");
      res.json("middleware 1");
      next();
    },
    (req, res, next) => {
      console.log("i am middleware 2");
      next();
    },
    (req, res, next) => {
      console.log("i am middleware 3");
    }
  )
  .get(
    (req, res, next) => {
      console.log("middleware 0");
      next("ram");
    },
    (err, req, res, next) => {
      console.log("middleware 1");
      next();
    },
    (req, res, next) => {
      console.log("middleware 2");
    },
    (err, req, res, next) => {
      console.log("middleware 3");
    }
  )
  .patch((req, res) => {
    res.json("i am patch method");
  })
  .delete((req, res) => {
    res.json("i am delete method");
  });

secondRouter
  .route("/a") // localhost:8000/a
  .post((req, res) => {
    res.json({
      success: true,
      message: "created",
    });
  })
  .get((req, res) => {
    // console.log("i am get method");
    res.json("i am get method");
  })
  .patch((req, res) => {
    res.json("i am patch method");
  })
  .delete((req, res) => {
    res.json("i am delete method");
  });

secondRouter
  .route("/a/:b/c/:id1") // localhost:8000/a/any/c
  .post((req, res) => {
    console.log(req.params);
    res.json({
      success: true,
      message: "*************",
    });
  })
  .get((req, res) => {
    // console.log("i am get method");
    res.json("i am get method");
  })
  .patch((req, res) => {
    res.json("i am patch method");
  })
  .delete((req, res) => {
    res.json("i am delete method");
  });

export default secondRouter;
