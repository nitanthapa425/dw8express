import { Router } from "express";
import { createPupil,  deletePupil,
    readAllPupil,
    readSpecificPupil,
    updatePupil, } from "../controller/pupilController.js";



export let pupilRouter = Router();



pupilRouter
  .route("/") //localhost:8000/pupils
  .post(createPupil)
  .get(readAllPupil);

pupilRouter
  .route("/:id") //localhost:8000/pupils/any
  .get(readSpecificPupil)
  .delete(deletePupil)
  .patch(updatePupil);

