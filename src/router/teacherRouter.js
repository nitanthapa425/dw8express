import { Router } from "express";

export let teacherRouter = Router();

teacherRouter
  .route("/")
  .post((req, res) => {
    res.json({
      success: true,
      message: "teachers creates successfully.",
    });
  })
  .get((req, res) => {
    res.json({
      success: true,
      message: "teachers read successfully.",
    });
  })
  .patch((req, res) => {
    res.json({
      success: true,
      message: "teachers updated successfully.",
    });
  })
  .delete((req, res) => {
    res.json({
      success: true,
      message: "teachers deleted successfully.",
    });
  });
