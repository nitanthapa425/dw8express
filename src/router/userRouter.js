import { createUser, deleteUser, readAllUser, readSpecificUser, updateUser } from "../controller/userController.js";
import { Router } from "express";


export let userRouter = Router();


userRouter
  .route("/") 
  .post(createUser)
  .get(readAllUser);

userRouter
  .route("/:id") 
  .get(readSpecificUser)
  .delete(deleteUser)
  .patch(updateUser);

