import { Router } from "express";

let firstRouter = Router();

//localhost:8000, post, get , patch, delete
//localhost:8000/store, post, get, patch, delete
//localhost:8000/store/chocolate, post, get, patch, delete

firstRouter
  .route("/") // localhost:8000
  .post((req, res) => {
    console.log(req.query);
    res.json({
      success: true,
      message: "created",
      data: req.body,
    });
  })
  .get((req, res) => {
    // console.log("i am get method");
    res.json("i am get method");
  })
  .patch((req, res) => {
    res.json("i am patch method");
  })
  .delete((req, res) => {
    res.json("i am delete method");
  });

firstRouter
  .route("/a") // localhost:8000/a
  .post((req, res) => {
    res.json({
      success: true,
      message: "created",
    });
  })
  .get((req, res) => {
    // console.log("i am get method");
    res.json("i am get method");
  })
  .patch((req, res) => {
    res.json("i am patch method");
  })
  .delete((req, res) => {
    res.json("i am delete method");
  });

firstRouter
  .route("/a/:b/c/:id1") // localhost:8000/a/any/c
  .post((req, res) => {
    console.log(req.params);
    res.json({
      success: true,
      message: "*************",
    });
  })
  .get((req, res) => {
    // console.log("i am get method");
    res.json("i am get method");
  })
  .patch((req, res) => {
    res.json("i am patch method");
  })
  .delete((req, res) => {
    res.json("i am delete method");
  });

export default firstRouter;

// url = localhost:8000
// method = post

//url = localhost:8000
// method = get

//url = localhost:8000
// method = patch

//url = localhost:8000
//method= patch

//c = create =>post
//r = read => get
//u = update => patch
//d  = delete => delete
