import { Router } from "express";
import { createProduct, deleteProduct, readAllProduct, readSpecificProduct, updateProduct } from "../controller/productController.js";

export let productRouter = Router();


productRouter
  .route("/") 
  .post(createProduct)
  .get(readAllProduct);

productRouter
  .route("/:id") 
  .get(readSpecificProduct)
  .delete(deleteProduct)
  .patch(updateProduct);

