import { Router } from "express";
import { Student } from "../schema/model.js";
import {
  createStudent,
  deleteStudent,
  readAllStudent,
  readSpecificStudent,
  updateStudent,
} from "../controller/studentController.js";

export let studentRouter = Router();

// await Student.create(data)
//await Student.find({}) => get all
//await Student.findById(id)
// await Student.findByIdAndDelete(id)
// await Student.findByIdAndUpdated(id,data)

//localhost:8000/students/651eae4889a8556c9bd1c3f0
//methode get

studentRouter
  .route("/") //localhost:8000/students
  .post(createStudent)
  .get(readAllStudent);

studentRouter
  .route("/:id") //localhost:8000/students/any
  .get(readSpecificStudent)
  .delete(deleteStudent)
  .patch(updateStudent);

//schema
//model
//router
//index

//postman

//localhost:8000/students/id
// methode dete
