import { Pupil } from "../schema/model.js";


export let createPupil = async (req, res) => {
  let data = req.body;

  try {
    let result = await Pupil.create(data);
    res.json({
      success: true,
      message: "pupils creates successfully.",
      data: result,
    });
  } catch (error) {
    res.json({
      success: false,
      message: error.message,
    });
  }
};

export let readAllPupil = async (req, res) => {
  try {
    // let result = await Pupil.find({});
    // let result = await Pupil.find({name:"nitan"});
    // let result = await Pupil.find({})
    // - -
    // 
    // except _id
    let result = await Pupil.find({}).select("age")

    
    res.json({
      success: true,
      message: "pupils read successfully.",
      data: result,
    });
  } catch (error) {
    res.json({
      success: false,
      message: error.message,
    });
  }
};

export let readSpecificPupil = async (req, res) => {
  let id = req.params.id;
  try {
    let result = await Pupil.findById(id);
    res.json({
      success: true,
      message: "pupil read successfully.",
      data: result,
    });
  } catch (error) {
    res.json({
      success: false,
      message: error.message,
    });
  }
};

export let deletePupil = async (req, res) => {
  let id = req.params.id;

  try {
    let result = await Pupil.findByIdAndDelete(id);

    res.json({
      success: true,
      message: "Pupil deleted successfully.",
      data: result,
    });
  } catch (error) {
    res.json({
      success: false,
      message: error.message,
    });
  }
};

export let updatePupil = async (req, res) => {
  let id = req.params.id;
  let data = req.body;

  try {
    let result = await Pupil.findByIdAndUpdate(id, data, { new: true });

    res.json({
      success: true,
      message: "pupil updated successfully.",
      data: result,
    });
  } catch (error) {
    res.json({
      success: false,
      message: error.message,
    });
  }
};

//deleteSpecificPupil
//updateSpecificPupil
// => id =>params
//  => data => body

//find weather the  two string is anagram or not
