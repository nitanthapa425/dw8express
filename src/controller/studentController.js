import { Student } from "../schema/model.js";

export let createStudent = async (req, res) => {
  let data = req.body;

  try {
    let result = await Student.create(data);
    res.json({
      success: true,
      message: "students creates successfully.",
      data: result,
    });
  } catch (error) {
    res.json({
      success: false,
      message: error.message,
    });
  }
};

export let readAllStudent = async (req, res) => {
  try {
    // let result = await Student.find({});
    // let result = await Student.find({name:"nitan"});
    // let result = await Student.find({})
    // - -
    // 
    // except _id
    let result = await Student.find({}).select("age")

    
    res.json({
      success: true,
      message: "students read successfully.",
      data: result,
    });
  } catch (error) {
    res.json({
      success: false,
      message: error.message,
    });
  }
};

export let readSpecificStudent = async (req, res) => {
  let id = req.params.id;
  try {
    let result = await Student.findById(id);
    res.json({
      success: true,
      message: "student read successfully.",
      data: result,
    });
  } catch (error) {
    res.json({
      success: false,
      message: error.message,
    });
  }
};

export let deleteStudent = async (req, res) => {
  let id = req.params.id;

  try {
    let result = await Student.findByIdAndDelete(id);

    res.json({
      success: true,
      message: "Student deleted successfully.",
      data: result,
    });
  } catch (error) {
    res.json({
      success: false,
      message: error.message,
    });
  }
};

export let updateStudent = async (req, res) => {
  let id = req.params.id;
  let data = req.body;

  try {
    let result = await Student.findByIdAndUpdate(id, data, { new: true });

    res.json({
      success: true,
      message: "student updated successfully.",
      data: result,
    });
  } catch (error) {
    res.json({
      success: false,
      message: error.message,
    });
  }
};

//deleteSpecificStudent
//updateSpecificStudent
// => id =>params
//  => data => body

//find weather the  two string is anagram or not
