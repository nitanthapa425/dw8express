/**
 *
 * express application (make)
 * attached port to that application
 */
import express, { json } from "express";
import firstRouter from "./src/router/firstRouter.js";
import customerRouter from "./src/router/customerRouter.js";
import secondRouter from "./src/router/secondRouter.js";
import connectToMongoDb from "./src/connectToDatabase/connectToMongoDB.js";
import { studentRouter } from "./src/router/studentRouter.js";
import { teacherRouter } from "./src/router/teacherRouter.js";
import { pupilRouter } from "./src/router/pupilRouter.js";
import { productRouter } from "./src/router/productRouter.js";
import { userRouter } from "./src/router/userRouter.js";
import { reviewRouter } from "./src/router/reviewRouter.js";
import cors from "cors";

//make express application
let expressApp = express();
expressApp.use(cors());
connectToMongoDb();

// expressApp.use(
//   (req, res, next) => {
//     console.log("i am app middleware 0");
//   },
//   (req, res, next) => {
//     console.log("i am app middleware 1");
//   }
// );

let port = 8000;
expressApp.use(json());
expressApp.use("/", firstRouter);
expressApp.use("/seconds", secondRouter);
expressApp.use("/students", studentRouter);
expressApp.use("/teachers", teacherRouter);

expressApp.use("/customers", customerRouter);
expressApp.use("/pupils", pupilRouter);
expressApp.use("/products", productRouter);
expressApp.use("/users", userRouter);
expressApp.use("/reviews", reviewRouter);
// attached port  to that application
expressApp.listen(port, () => {
  console.log(`app is connected at port ${port} successfully`);
});

//**** */
// to create api
// frontend request       =>    backend response

// one request  => response
// two request  => response

//connect our application to mongodb database
